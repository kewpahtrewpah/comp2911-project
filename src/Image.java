import javax.imageio.ImageIO;
import javax.swing.JPanel;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Image Class to build a JPanel from an image
 * Loads specified file and builds panel
 */
public class Image extends JPanel {
	
	private BufferedImage image;
	
	/**
	 * Image Constructor
	 * @param file
	 */
	public Image(String file) {
		try {                
			image = ImageIO.read(new File(file));
		} catch (IOException e) {
			System.out.println("Could not read file.");
			e.printStackTrace();
		}
	}

	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);
    }
}
