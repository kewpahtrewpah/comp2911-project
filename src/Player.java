/**
 * Enumerator for player pieces
 */
public enum Player {
	PLAYER1, PLAYER2, CPU, HUMAN, NONE
}
