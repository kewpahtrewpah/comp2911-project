import java.io.File;
import javax.sound.sampled.*;

/**
 * SFX Class for .wav playback
 * loads a .wav file specified by parameter string
 */
public class SFX {
	private File sFile;
	private Clip sClip;
	
	/**
	 * SFX constructor
	 * @param filePath
	 */
	public SFX(String filePath) {
		try {
			sFile = new File(filePath);
			AudioInputStream sound = AudioSystem.getAudioInputStream(sFile);
			DataLine.Info info = new DataLine.Info(Clip.class, sound.getFormat());
			sClip = (Clip) AudioSystem.getLine(info);	
			sClip.open(sound);
		} catch (Exception e) {
			System.out.println("Could not construct SFX");
			e.printStackTrace();
		}
	}
	
	public void play() {
		sClip.setFramePosition(0);
		sClip.start();
	}
	
	public void stop() {
		sClip.stop();
		sClip.setFramePosition(0);
	}
}
