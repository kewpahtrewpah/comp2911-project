import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Random;
import java.util.TimerTask;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.swing.*;

import java.util.Timer;

/**
 *	Master Class: handles main frame, UI, inputs, modes/AI and game flow
 */
public class GameMaster implements ActionListener {
	public static void main(String[] Args) {
		GameMaster theMaster = new GameMaster();
	}
	
	/**
	 * Build GameMaster
	 */
	public GameMaster() {
		// Setup Game
		mainFrame = new JFrame("Connect 4 In Space");
		difficulty = Difficulty.EASY;
		inGame = false;
		gravMode = false;
		randomGrav = false; //true is random, false is clockwise
		AI = false; 
		if (AI) currPlayer = Player.HUMAN;
		else currPlayer = Player.PLAYER1;
		setupSFX();
		
		// Panels
		JPanel splashPanel = new Image("img/splash.png");
		splashPanel.setOpaque(true);
		JPanel helpPanel = new Image("img/help.png");
		helpPanel.setOpaque(true);
		JPanel creditsPanel = new Image("img/credits.png");
		creditsPanel.setOpaque(true);
		JPanel p1winPanel = new Image("img/p1win.png");
		p1winPanel.setOpaque(true);
		JPanel p2winPanel = new Image("img/p2win.png");
		p2winPanel.setOpaque(true);
		JPanel humanWinPanel = new Image("img/won.png");
		humanWinPanel.setOpaque(true);
		JPanel AIWinPanel = new Image("img/lost.png");
		AIWinPanel.setOpaque(true);
		JPanel NoneWinPanel = new Image("img/tie.png");
		AIWinPanel.setOpaque(true);
		
		// size of board stuff
		position = (int) Math.ceil(7/2);
		board = new Board(7,6);
		gPanel = new GamePanel(7,6,this);
		
		// Setup Cards
		mainLayout = new JPanel(new CardLayout());
		mainLayout.setPreferredSize(new Dimension(750,600));
		mainLayout.add(splashPanel, "SPLASH_PANEL");
		mainLayout.add(gPanel, "GAME_PANEL");
		mainLayout.add(helpPanel, "HELP_PANEL");
		mainLayout.add(creditsPanel, "CREDITS_PANEL");
		mainLayout.add(p1winPanel, "P1WIN_PANEL");
		mainLayout.add(p2winPanel, "P2WIN_PANEL");
		mainLayout.add(humanWinPanel, "HWIN_PANEL");
		mainLayout.add(AIWinPanel, "AI_WIN_PANEL");
		mainLayout.add(NoneWinPanel, "NONE_WIN_PANEL");
		mainLayout.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                click();
            }
        });
		
		iPanel = new InfoPanel(AI, currPlayer);
		renderPanel = new JPanel(new BorderLayout());
		renderPanel.add(mainLayout, BorderLayout.CENTER);
		renderPanel.add(iPanel, BorderLayout.EAST);
		iPanel.setVisible(false);
		renderPanel.setPreferredSize(new Dimension(750,600));
		// Setup Keyboard Input
		addKeys();
		
		// Setup Main Frame
		JMenuBar dropMenu = this.createMenuBar();
		menuBar.getMenu(1).setEnabled(false);
		mainFrame.setJMenuBar(dropMenu);
		mainFrame.setResizable(false);
		mainFrame.getContentPane().add(renderPanel);
		mainFrame.pack();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setVisible(true);
	}

	//////////////////////////////////////////////////////////////////////////////////
	// Game Flow
	//////////////////////////////////////////////////////////////////////////////////
	/**
	 *  New Game
	 *  Build new game according to current game settings
	 */
	private void newGame() {
		if (musicOn) music.start();
		menuBar.getMenu(2).setEnabled(true);
		if (AI) {
			menuBar.getMenu(1).setEnabled(true);
			currPlayer = Player.HUMAN;
		} else {
			currPlayer = Player.PLAYER1;
			menuBar.getMenu(1).setEnabled(false);
		}
		if (!inGame) inGame = true;
		board.newGame(board.getNCol(),board.getNRow());
		gPanel.newGame();
		board.setDirection(1);
		iPanel.updateMode(AI, gravMode, randomGrav);
		iPanel.updatePlayer(currPlayer);
		iPanel.updateStatus(board.getDirection(), board.getNextDirection(), 3-board.getTurn()%3, gravMode);
		showGame();
	}
	
	/**
	 * doTurn
	 * This function handles placement of piece onto board
	 * checkWin is called after each move is place as well as after gravity cycle
	 * If up against CPU, AI selection will be made
	 */
    private void doTurn() {
    	Point move = board.makeMove(position, currPlayer);
    	// if valid move
		if (move.x >= 0) {
			board.incrementTurn();
			//update GamePanel
			gPanel.makeMove(move, currPlayer);
			if (checkWin()) return;
			
			if (!AI) { // HVH
				if (currPlayer == Player.PLAYER1) {
					currPlayer = Player.PLAYER2;
				} else {
					currPlayer = Player.PLAYER1;
				}
				iPanel.updatePlayer(currPlayer);
				placeSFX.play();
				if (board.isFull()) showWin(Player.NONE);
			} else { // CPU/AI
				currPlayer = Player.CPU;
				iPanel.updatePlayer(Player.CPU);
				// CPU thinking sequence
				Random n = new Random();
				Timer timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						AIControl();
						iPanel.updatePlayer(currPlayer);
						iPanel.updateStatus(board.getDirection(), board.getNextDirection(), 3-board.getTurn()%3, gravMode);
						if (gravMode) shiftGravity();
						if (board.isFull()) showWin(Player.NONE);
						currPlayer = Player.HUMAN;
						placeSFX.play();
					}
				}, Math.abs(n.nextInt() % 2000));
			}
			
			if (gravMode) shiftGravity();
			//players can win on their other move during gravity mode 
			checkWin();
		} else { // badMove
			iPanel.badMove(currPlayer);
			badSFX.play();
		}
    }
    
    /**
     * checkWin is a function to check if any player has won
     * displays winner panel if so
     * @return boolean win
     */
    private boolean checkWin() {
		if (AI) {
			if (board.checkWin(Player.HUMAN)) {
				showWin(Player.HUMAN);
				return true;
			} else if (board.checkWin(Player.CPU)) {
				showWin(Player.CPU);
				return true;
			}
		} else {
			if (board.checkWin(Player.PLAYER1)) {
				showWin(Player.PLAYER1);
				return true;
			} else if (board.checkWin(Player.PLAYER2)) {
				showWin(Player.PLAYER2);
				return true;
			}
		}
		return false;
	}
    
    /**
     * AIControl
     * TODO
     */
    private void AIControl() {
    	CPU AI = new CPU(difficulty, board);
    	Point move = AI.nextMove(); //move is implemented in this function
    	if (move.x >= 0 && move.y >= 0) {
    		Point actualMove = new Point();
    		if (board.getDirection()%2 == 1) {
    			actualMove = board.makeMove(move.x, Player.CPU);
    		} else {
    			actualMove = board.makeMove(move.y, Player.CPU);
    		}
    		if (actualMove.x >= 0) {
    			gPanel.makeMove(actualMove, Player.CPU);
        		board.incrementTurn();
        		checkWin();
    		}
    	}
    	
    }
    
    /**
     * shiftGravity handles gravity fields as well as updating board when shifted
     * Gravity shifts every 3 turns
     */
    private void shiftGravity() {
		if (board.getTurn() % 3 == 0) {
			//set next direction, either random or clockwise
			if (randomGrav) {
				int oldDir = board.getDirection();
				board.setDirection(board.getNextDirection());
				Random n = new Random();
				int nextDir;
				do {
					nextDir = Math.abs(n.nextInt() % 4);
					nextDir++;
				} while (n.nextInt() == oldDir);
				board.setNextDirection(nextDir);
			} else {
				board.setDirection(board.getDirection() + 1);
				if (board.getDirection() == 5) {
					board.setDirection(1);
				}
				board.setNextDirection(board.getDirection() + 1);
				if (board.getNextDirection() == 5) {
					board.setNextDirection(1);
				}
			}
			//shift to next direction
			board.shift(board.getDirection());
			gPanel.updateBoard(board);
			
			if (board.getDirection()%2 == 1) {
				position = (int) Math.ceil(board.getNCol()/2);
			} else {
				position = (int) Math.ceil(board.getNRow()/2);
			}
			gPanel.setProjection(position, board.getDirection());
			gravitySFX.play();
		}
		iPanel.updateStatus(board.getDirection(), board.getNextDirection(), 3-board.getTurn()%3, gravMode);
    }
    
	//////////////////////////////////////////////////////////////////////////////////
	// Input related
	//////////////////////////////////////////////////////////////////////////////////
    /**
     * Function called by Cell to update projection when hovered over
     * @param p Point (co-ordinates) of hovered Cell
     */
    public void hoverProjection(Point p) {
    	if (board.getDirection()%2 == 1) {
			position = p.x;
		} else {
			position = p.y;
		}
		gPanel.setProjection(position, board.getDirection());
    }
    
    /**
     * Called when spacebar or mouseclick detected
     * doTurn according to current projection (from mouse or keyboard)
     * newGame if not inGame
     */
    public void click() {
    	if (inGame) {
    		if (AI) {
    			if (currPlayer == Player.HUMAN) doTurn();
    		}
    		else {
    			doTurn();
    		}
		}
    	else {
        	newGame();
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    // Set up
    //////////////////////////////////////////////////////////////////////////////////
    /**
     * Sound related setup
     * Loads MIDI file for music and sound files for sound
     */
    private void setupSFX() {
		musicOn = true;
		try {
			File musicFile = new File("sfx/tyrian.mid");
			Sequence sequence = MidiSystem.getSequence(musicFile);
			music = MidiSystem.getSequencer();
			music.open();
			music.setSequence(sequence);
	    }
		catch (Exception e) {
			System.out.println("Could not build music.");
			e.printStackTrace();
		}
		music.setTickPosition(0);
		music.start();
		winSFX = new SFX("sfx/win.wav");
		loseSFX = new SFX("sfx/lose.wav");
		gravitySFX = new SFX("sfx/gravity.wav");
		placeSFX = new SFX("sfx/place.wav");
		badSFX = new SFX("sfx/bad.wav");
	}
    
    /**
     * Keyboard setup
     * inputs include left/right/down/up(projection selection), spacebar(select) and enter(newGame)
     */
    private void addKeys() {		
    	mainLayout.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "left");
		mainLayout.getActionMap().put("left", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				if (inGame) {
					if (board.getDirection()%2 == 1) {
						if (position == 0) position = board.getNCol()-1;
						else position--;
					} else {
						if (position == 0) position = board.getNRow()-1;
						else position--;
					}
					gPanel.setProjection(position, board.getDirection());
				}
			}
		});
		
		mainLayout.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "right");
		mainLayout.getActionMap().put("right", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				if (inGame) {
					if (board.getDirection()%2 == 1) {
						if (position == board.getNCol()-1) position = 0;
						else position++;
					} else {
						if (position == board.getNRow()-1) position = 0;
						else position++;
					}
					gPanel.setProjection(position, board.getDirection());
				}
			}
		});
		
		mainLayout.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "up");
		mainLayout.getActionMap().put("up", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				if (inGame) {
					if (board.getDirection()%2 == 0) {
						if (position == board.getNCol()-1) position = 0;
						else position++;
					} else {
						if (position == board.getNRow()-1) position = 0;
						else position++;
					}
					gPanel.setProjection(position, board.getDirection());
				}
			}
		});
		
		mainLayout.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "down");
		mainLayout.getActionMap().put("down", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				if (inGame) {
					if (board.getDirection()%2 == 0) {
						if (position == 0) position = board.getNCol()-1;
						else position--;
					} else {
						if (position == 0) position = board.getNRow()-1;
						else position--;
					}
					gPanel.setProjection(position, board.getDirection());
				}
			}
		});
		
		mainLayout.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), "select");
		mainLayout.getActionMap().put("select", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				click();
			}
		});
		
		mainLayout.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "showGame");
		mainLayout.getActionMap().put("showGame", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				if (!inGame) {
					newGame();
				} else {
					showGame();
				}
			}
		});
	}
    
	//////////////////////////////////////////////////////////////////////////////////
	// User Interface
	//////////////////////////////////////////////////////////////////////////////////
 	/**
 	 * Shows the Help Screen
 	 */
 	private void showHelp() {	
 		iPanel.setVisible(false);
 		CardLayout cardLayout = (CardLayout) mainLayout.getLayout();
 		cardLayout.show(mainLayout, "HELP_PANEL");
 	}
 	/**
 	 * Shows the Splash Screen
 	 */
 	private void showSplash() {
 		iPanel.setVisible(false);
 		cardLayout = (CardLayout) mainLayout.getLayout();
 		cardLayout.show(mainLayout,"SPLASH_PANEL");
 	}
 	/**
 	 * Shows the Credits Screen
 	 */
 	private void showCredits() {
 		iPanel.setVisible(false);
 		cardLayout = (CardLayout) mainLayout.getLayout();
 		cardLayout.show(mainLayout,"CREDITS_PANEL");
 	}
 	/**
 	 * Shows the Game Screen
 	 */
 	private void showGame() {
 		iPanel.setVisible(true);
 		cardLayout = (CardLayout) mainLayout.getLayout();
 		cardLayout.show(mainLayout,"GAME_PANEL");
 		gPanel.setProjection(position, board.getDirection());
 	}

 	/**
 	 * Shows the Winner Screen
 	 */
 	private void showWin(Player p) {
 		menuBar.getMenu(1).setEnabled(false);
 		music.stop();
 		cardLayout = (CardLayout) mainLayout.getLayout();
 		if (p == Player.PLAYER1) cardLayout.show(mainLayout,"P1WIN_PANEL");
 		else if (p == Player.PLAYER2) cardLayout.show(mainLayout,"P2WIN_PANEL");
 		else if (p == Player.HUMAN) cardLayout.show(mainLayout,"HWIN_PANEL");
 		else if (p == Player.CPU) cardLayout.show(mainLayout,"AI_WIN_PANEL");
 		else if (p == Player.NONE) cardLayout.show(mainLayout,"NONE_WIN_PANEL");
 		inGame = false;
 		iPanel.setVisible(false);
 		if (p == Player.CPU) loseSFX.play();
 		else winSFX.play();
 	}
 	
 	/**
	 * JMenuBar constructor
	 * Hotkeys include:
	 * alt+1 Human vs Human
	 * alt+2 Human vs AI
	 * alt+3 board size
	 * alt+4 quit
	 * ctrl+(1-4) difficulty 
	 * @return JMenuBar
	 */
	private JMenuBar createMenuBar() {
        //Create the menu bar.
        this.menuBar = new JMenuBar();
 
        //Build the main menu
        JMenu gameMenu = new JMenu("Game");
        menuBar.add(gameMenu);
        
        ButtonGroup group = new ButtonGroup();
 
        JRadioButtonMenuItem humanVsHumanItem = new JRadioButtonMenuItem("Human vs Human", false);
        humanVsHumanItem.setAccelerator(KeyStroke.getKeyStroke('1', KeyEvent.ALT_DOWN_MASK));
        humanVsHumanItem.addActionListener(this);
        gameMenu.add(humanVsHumanItem);
        group.add(humanVsHumanItem);
        
        JRadioButtonMenuItem humanVsAlienItem = new JRadioButtonMenuItem("Humans vs Aliens", false);
        humanVsAlienItem.setAccelerator(KeyStroke.getKeyStroke('2', KeyEvent.ALT_DOWN_MASK));
        humanVsAlienItem.addActionListener(this);
        gameMenu.add(humanVsAlienItem);
        group.add(humanVsAlienItem);

        JMenuItem boardItem = new JMenuItem("Resize Board");
        boardItem.setAccelerator(KeyStroke.getKeyStroke('3', KeyEvent.ALT_DOWN_MASK));
        boardItem.addActionListener(this);
        gameMenu.add(boardItem);
        
        JMenuItem exitItem = new JMenuItem("Exit");
        exitItem.setAccelerator(KeyStroke.getKeyStroke('4', KeyEvent.ALT_DOWN_MASK));
        exitItem.addActionListener(this);
        gameMenu.add(exitItem);
 
        //Build the difficulty menu
        JMenu difficultyMenu = new JMenu("Difficulty");
        menuBar.add(difficultyMenu);
        
        group = new ButtonGroup();
        
        //easy
        JRadioButtonMenuItem childsplayItem = new JRadioButtonMenuItem("Childsplay",true);
        childsplayItem.setAccelerator(KeyStroke.getKeyStroke('1', KeyEvent.CTRL_DOWN_MASK));
        childsplayItem.addActionListener(this);
        difficultyMenu.add(childsplayItem);
        group.add(childsplayItem);
 
        //medium
        JRadioButtonMenuItem mediocreItem = new JRadioButtonMenuItem("Mediocre",false);
        mediocreItem.setAccelerator(KeyStroke.getKeyStroke('2', KeyEvent.CTRL_DOWN_MASK));
        mediocreItem.addActionListener(this);
        difficultyMenu.add(mediocreItem);
        group.add(mediocreItem);
        
        //hard
        JRadioButtonMenuItem impossibleItem = new JRadioButtonMenuItem("Impossible",false);
        impossibleItem.setAccelerator(KeyStroke.getKeyStroke('3', KeyEvent.CTRL_DOWN_MASK));
        impossibleItem.addActionListener(this);
        difficultyMenu.add(impossibleItem);
        group.add(impossibleItem);
        
        //very hard
        JRadioButtonMenuItem slaughterhouseItem = new JRadioButtonMenuItem("Slaughterhouse",false);
        slaughterhouseItem.setAccelerator(KeyStroke.getKeyStroke('4', KeyEvent.CTRL_DOWN_MASK));
        slaughterhouseItem.addActionListener(this);
        difficultyMenu.add(slaughterhouseItem);
        group.add(slaughterhouseItem);
        
        //Build the Setings menu
        JMenu settingsMenu = new JMenu("Settings");
        menuBar.add(settingsMenu);
        
        JRadioButtonMenuItem gravityModeItem = new JRadioButtonMenuItem("Gravity Mode", this.gravMode);
        gravityModeItem.addActionListener(this);
        settingsMenu.add(gravityModeItem);
        
        JRadioButtonMenuItem randomGravityItem = new JRadioButtonMenuItem("Random Gravity", this.randomGrav);
        randomGravityItem.addActionListener(this);
        settingsMenu.add(randomGravityItem);
        
        JRadioButtonMenuItem musicItem = new JRadioButtonMenuItem("Music", this.musicOn);
        musicItem.addActionListener(this);
        settingsMenu.add(musicItem);
        
        //Build the Help menu
        JMenu helpMenu = new JMenu("Help");
        menuBar.add(helpMenu);
        
        JMenuItem helpItem = new JMenuItem("Help");
        helpItem.addActionListener(this);
        helpMenu.add(helpItem);
        
        JMenuItem creditsItem = new JMenuItem("Credits");
        creditsItem.addActionListener(this);
        helpMenu.add(creditsItem);

        return menuBar;
    }
	
	
	/**
	 * MenuBar selections
	 * @param ActionEvent dropMenu action
	 */
    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        String menuText = source.getText();
//        switch(menuText) {
        if (menuText.equals("Exit")) {
        	System.exit(0);
        } else if (menuText.equals("Human vs Human") || menuText.equals("Humans vs Aliens")){
        	if (menuText.equals("Human vs Human")) {
        		this.AI = false;
        	} else {
        		this.AI = true;
        	}
        	newGame();
        } else if (menuText.equals("Resize Board")){
            String nColStr = JOptionPane.showInputDialog(
                    "How many rows?", null);
            String nRowStr = JOptionPane.showInputDialog(
                    "How many columns?", null);
            int nColInt = 4, nRowInt = 4;
            boolean success = false;
            try { 
                nColInt= Integer.parseInt(nColStr);
                nRowInt= Integer.parseInt(nRowStr);
                if (nColInt >= 4 && nColInt <= 10 && nRowInt >= 4 && nRowInt <= 10) success = true;
            } catch(NumberFormatException e1) { 
            	JOptionPane.showMessageDialog(null, "Please insert acceptable dimensions (greater than 4x4, less than 10x10)", "Please insert acceptable dimensions (greater than 4x4, less than 10x10)!", JOptionPane.ERROR_MESSAGE);
            }
            if (success) {
            	position = (int) Math.ceil(nColInt/2);
            	board.newGame(nColInt,nRowInt);
            	mainLayout.remove(gPanel);
            	gPanel = new GamePanel(nColInt,nRowInt, this);
        		mainLayout.add(gPanel, "GAME_PANEL");
        		newGame();
        		showGame();
            }
        } else if (menuText.equals("Childsplay")) {
        	difficulty = Difficulty.EASY;
        } else if (menuText.equals("Mediocre")) {
        	difficulty = Difficulty.MEDIUM;
        } else if (menuText.equals("Impossible")) {
        	difficulty = Difficulty.HARD;
        } else if (menuText.equals("Slaughterhouse")) {
        	difficulty = Difficulty.VERYHARD; 
        } else if (menuText.equals("Help")) {
        	showHelp();
        } else if (menuText.equals("Credits")) {
        	showCredits();
        } else if (menuText.equals("Gravity Mode")) {
        	if (this.gravMode) {
        		this.gravMode = false;
        	} else {
        		this.gravMode = true;
        	}
        	newGame();
        } else if (menuText.equals("Random Gravity")) {
        	if (this.randomGrav) {
        		this.randomGrav = false;
        	} else {
        		this.randomGrav = true;
        	}
        	if (this.gravMode) newGame();
        } else if (menuText.equals("Music")) {
        	if (musicOn) {
        		musicOn = false;
        		music.stop();
        		music.setTickPosition(0);
        	} else {
        		musicOn = true;
        		music.setTickPosition(0);
        		music.start();
        	}
        }
    }
    
    // private fields
    private JFrame mainFrame;
	private JPanel mainLayout;
	private JPanel renderPanel;
	private GamePanel gPanel;
	private InfoPanel iPanel;
	private Board board;
	private CardLayout cardLayout;
	private JMenuBar menuBar;
	private Difficulty difficulty;
	private int position;
	private Boolean inGame;
	private Player currPlayer;
	private boolean musicOn;
	private Sequencer music;
	private boolean gravMode;
	private boolean randomGrav;
	private boolean AI;
	private SFX winSFX;
	private SFX loseSFX;
	private SFX gravitySFX;
	private SFX placeSFX;
	private SFX badSFX;
}
