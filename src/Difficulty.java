/**
 * Enumerator for AI difficulty levels
 */
public enum Difficulty {
	EASY, MEDIUM, HARD, VERYHARD
}
