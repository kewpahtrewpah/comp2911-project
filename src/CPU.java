import java.awt.Point;
import java.util.Random;


public class CPU {
    private Difficulty difficulty;
    private Board b;
//    private Board b1;
    
    static final int MAX_DEPTH = 16;
    static final float WIN = 1f;
    static final float LOSS = -1f;
    static final float UNCERTAINTY = 0f;
    
    public CPU(Difficulty difficulty, Board b) {
        this.difficulty = difficulty;
        this.b = b;
    }
    
    //perform next AI move
    public Point nextMove() {
        Point move = new Point();
        int direction = b.getDirection();
        if (difficulty == difficulty.EASY) {
            move = randomMove(direction);
        } else if (difficulty == difficulty.MEDIUM) {
            if (canWin(true).x >= 0) {
                move = canWin(true);
            } else if (canWin(false).x >= 0) {
                move = canWin(false);
            } else {
                move = randomMove(direction);
            }
        } else if (difficulty == difficulty.HARD) {
            if (bottomRowHack().x>=0  && b.getTurn() <= 5) {
                move = bottomRowHack();
            } else {
                move = minimaxBestMove();
            }
        } else if (difficulty == difficulty.VERYHARD) {
            if (bottomRowHack().x>=0 && b.getTurn() <= 5) {
                move = bottomRowHack();
            } else if (canWin(true).x >= 0) {
                move = canWin(true);
            } else if (canWin(false).x >= 0) {
                move = canWin(false);
            } else {
                move = minimaxBestMove();
            }
        }
        
        return move;
    }
    
    private Point randomMove(int direction) {
        Point move = new Point(-1,-1);
        Random n = new Random();
        do {
            move.x = Math.abs(n.nextInt() % b.getNCol());
            move.y = Math.abs(n.nextInt() % b.getNRow());
        } while (b.getBoard()[move.x][move.y] != Player.NONE);
        return move;
    }
    
    private Point bottomRowHack() {
        for (int x = 2; x < b.getNCol()-2; x++) {
            int count = 0;
            if (b.getBoard()[x][0] == Player.HUMAN) count++;
            if (b.getBoard()[x+1][0] == Player.HUMAN) count++;
            if (count == 2 && b.getBoard()[x+2][0] == Player.NONE) return new Point(x+2,0);
            else count = 0;
        }
        return new Point(-1,-1);
    }
    
    //finds out the move that a player (CPU or human) can win at on their next turn
    private Point canWin(boolean CPU) {
        Player player;
        if (CPU) {
            player = Player.PLAYER2;
        } else {
            player = Player.PLAYER1;
        }
        
        int count = 0;
        Point winner = new Point(-1,-1);
        for (int x = 0; x < b.getNCol(); x++) {
            for (int y = 0; y < b.getNRow()-3; y++) {
                Point link = new Point(-1,-1);
                count = 0;
                if (b.getBoard()[x][y+3] == player) count++; else link = new Point(x,y+3);
                if (b.getBoard()[x][y+2] == player) count++; else link = new Point(x,y+2);
                if (b.getBoard()[x][y+1] == player) count++; else link = new Point(x,y+1);
                if (b.getBoard()[x][y] == player) count++; else link = new Point(x,y);
                if (count == 3 && link.x != -1 && link.y != -1) {
                    if (link.y == 0) {
                        if ( b.getBoard()[link.x][0] == Player.NONE) {
                            winner = link;
                        }
                    } else if (b.getBoard()[link.x][link.y-1] != Player.NONE && b.getBoard()[link.x][link.y] == Player.NONE) {
                        winner = link;
                    }
                }
            }
        }
        
        count = 0;
        for (int y = 0; y < b.getNRow(); y++) {
            for (int x = 0; x < b.getNCol()-3; x++) {
                Point link = new Point(-1,-1);
                count = 0;
                if (b.getBoard()[x+3][y] == player) count++; else link = new Point(x+3,y);
                if (b.getBoard()[x+2][y] == player) count++; else link = new Point(x+2,y);
                if (b.getBoard()[x+1][y] == player) count++; else link = new Point(x+1,y);
                if (b.getBoard()[x][y] == player) count++; else link = new Point(x,y);
                if (count == 3 && link.x != -1 && link.y != -1) {
                    if (link.y == 0) {
                        if ( b.getBoard()[link.x][0] == Player.NONE) {
                            winner = link;
                        }
                    } else if (b.getBoard()[link.x][link.y-1] != Player.NONE && b.getBoard()[link.x][link.y] == Player.NONE) {
                        winner = link;
                    }
                }
            }
        }
        
        count = 0;
        for (int y = 0; y < b.getNRow() - 3; y++) {
            for (int x = 0; x < b.getNCol() - 3; x++) {
                Point link = new Point(-1,-1);
                count = 0;
                if (b.getBoard()[x+3][y+3] == player) count++; else link = new Point(x+3,y+3);
                if (b.getBoard()[x+2][y+2] == player) count++; else link = new Point(x+2,y+2);
                if (b.getBoard()[x+1][y+1] == player) count++; else link = new Point(x+1,y+1);
                if (b.getBoard()[x][y] == player) count++; else link = new Point(x,y);
                if (count == 3 && link.x != -1 && link.y != -1) {
                    if (link.y == 0) {
                        if ( b.getBoard()[link.x][0] == Player.NONE) {
                            winner = link;
                        }
                    } else if (b.getBoard()[link.x][link.y-1] != Player.NONE && b.getBoard()[link.x][link.y] == Player.NONE) {
                        winner = link;
                    }
                }
            }
        }
        
        count = 0;
        for (int y = 0; y < b.getNRow() - 3; y++) {
            for (int x = 3; x < b.getNCol(); x++) {
                Point link = new Point(-1,-1);
                count = 0;
                if (b.getBoard()[x-3][y+3] == player) count++; else link = new Point(x-3,y+3);
                if (b.getBoard()[x-2][y+2] == player) count++; else link = new Point(x-2,y+2);
                if (b.getBoard()[x-1][y+1] == player) count++; else link = new Point(x-1,y+1);
                if (b.getBoard()[x][y] == player) count++; else link = new Point(x,y);
                if (count == 3 && link.x != -1 && link.y != -1) {
                    if (link.y == 0) {
                        if ( b.getBoard()[link.x][0] == Player.NONE) {
                            winner = link;
                        }
                    } else if (b.getBoard()[link.x][link.y-1] != Player.NONE && b.getBoard()[link.x][link.y] == Player.NONE) {
                        winner = link;
                    }
                }
            }
        }
        
        return winner;
    }
    
    private Point minimaxBestMove() {
        
        double maxValue = 2*Integer.MIN_VALUE;
        Point move = new Point(0,0);
        
        for (int x = 0; x < b.getNCol() && x < b.getNRow(); x++) {
            if (b.isValidMove(x)) {
                double value = moveValue(x);
                if (value > maxValue) {
                    maxValue = value;
                    move = b.makeMove(x, Player.CPU);
                    b.undoMove(move);
                    if (value == WIN) {
                        break;
                    }
                }
            }
        }
        return move;
    }
    
    private double moveValue (int x) {
        Point m = b.makeMove(x, Player.CPU);
        double value = alphabeta(MAX_DEPTH, Integer.MIN_VALUE, Integer.MAX_VALUE, false);
        b.undoMove(m);
        return value;
    }
    
    private double alphabeta(int depth, double alpha, double beta, boolean maxPlayer) {
        boolean hasWon = false;
        if (b.checkWin(Player.CPU)) {
            hasWon = true;
        } else if (b.checkWin(Player.HUMAN)) {
            hasWon = true;
        }
        if (depth == 0 || hasWon) {
            double score = 0;
            if (hasWon) {
                score = b.checkWin(Player.HUMAN) ? LOSS : WIN;
            } else {
                score = UNCERTAINTY;
            }
            return score/(MAX_DEPTH-depth+1);
        }
        
        if (maxPlayer) {
            for (int x = 0; x < b.getNCol() && x<b.getNRow(); x++) {
                if (b.isValidMove(x)) {
                    Point m = b.makeMove(x, Player.CPU);
                    alpha = Math.max(alpha, alphabeta(depth-1,alpha,beta,false));
                    b.undoMove(m);
                    if (beta <= alpha) {
                        break;
                    }
                }
            }
            return alpha;
        } else {
            for (int x = 0; x < b.getNCol() && x< b.getNRow(); x++) {
                if (b.isValidMove(x)) {
                    Point m = b.makeMove(x, Player.CPU);
                    beta = Math.min(beta, alphabeta(depth-1,alpha,beta,true));
                    b.undoMove(m);
                    if (beta <= alpha) {
                        break;
                    }
                }
            }
            return beta;
        }
    }
}
