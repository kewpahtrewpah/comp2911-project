import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * Cell class for board rendering
 * Cell contains blue background and circular player piece (or empty)
 * Projection is handled by rendering a cyan background
 * Mouse inputs are handled here and require a link to GameMaster
 * Board is composed of a grid of cells
 */
public class Cell extends JPanel {
    private Player type;
    private Boolean projection;
    private GameMaster gm;
    private int x, y;
    
    /**
     * Cell constructor
     * @param x
     * @param y
     * @param projection
     * @param gm
     */
    public Cell (final int x, final int y, Boolean projection, final GameMaster gm) {
    	this.x = x;
    	this.y = y;
    	this.gm = gm;
    	this.type = Player.NONE;
    	this.projection = projection;
        setPreferredSize(new Dimension(50,50));
        
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                gm.click();
            }
            @Override
            public void mouseEntered(MouseEvent e) {
            	gm.hoverProjection(new Point(x, y));
            }
        });
        
        repaint();
    }
    
    @Override
    public void paintComponent(Graphics g) {
		Graphics2D component = (Graphics2D) g;            
	    super.paintComponent(component);
	    if (projection) {
	    	setBackground(Color.cyan);
	    } else {
	    	setBackground(Color.blue);
	    }
	    if (type == Player.PLAYER1 || type == Player.HUMAN) {
	    	g.setColor(Color.red);
	    } else if (type == Player.PLAYER2 || type == Player.CPU) {
	    	g.setColor(Color.yellow);
	    } else {
	    	g.setColor(Color.black);
	    }
	    g.fillRoundRect(5, 5, getWidth() - 10, getHeight() - 10, getWidth(), getHeight());
	    g.setColor(Color.black);
	    g.drawRoundRect(5, 5, getWidth() - 10, getHeight() - 10, getWidth(), getHeight());
	}
    
    /**
     * Update if Cell is selected
     * @param projection
     */
    public void setProjection(Boolean b) {
    	this.projection = b;
    	repaint();
	}
    
    /**
     * Is Cell selected
     * @return projection
     */
    public boolean isProjection() { return projection; }
    
    /**
     * Update Cell with player type
     * @param type
     */
    public void setType(Player type) { 
    	this.type = type;
    	repaint();
	}
    /**
     * Return player type
     * @return type
     */
    public Player type() { return this.type; }
}
