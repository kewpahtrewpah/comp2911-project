import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * GamePanel Class contains rendering portion of board
 * Board render is made of a grid of cells built according to board dimensions/status
 */
public class GamePanel extends JPanel {
	private Cell boardRender[][];
	private int nCols;
	private int nRows;
	private int projection;
	
	/**
	 * GamePanel Constructor
	 * @param cols
	 * @param rows
	 * @param gm GameMaster required for mouse input in Cells
	 */
	public GamePanel(int cols, int rows, GameMaster gm) {
		nCols = cols;
		nRows = rows;
		boardRender = new Cell[nCols][nRows];
		projection = (int) Math.ceil(nCols / 2);
		setLayout(new GridLayout(nRows, nCols));
		setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		setBackground(Color.black);
		//insert to grid
		for (int y = nRows-1; y >= 0; y--) {
			for (int x = 0; x < nCols; x++) {
				if (x == projection) {
					Cell c = new Cell(x, y, true, gm);
					boardRender[x][y] = c;
					add(c);
				} else {
					Cell c = new Cell(x, y, false, gm);
					boardRender[x][y] = c;
					add(c);
				}
			}
		}
	}
	
	/**
	 * newGame set all pieces to NONE and no projection
	 */
	public void newGame() {
		for (int y = 0; y < nRows; y++) {
			for (int x = 0; x < nCols; x++) {
				boardRender[x][y].setType(Player.NONE);
				boardRender[x][y].setProjection(false);
			}
		}
	}
	
	/**
	 * Update projection
	 * @param position
	 * @param direction
	 */
	public void setProjection(int position, int direction) {
		for (int x = 0; x < nCols; x++) {
			for (int y = 0; y < nRows; y++) {
				boardRender[x][y].setProjection(false);
			}
		}
		if (direction == 1 || direction == 3) {
			this.projection = position;
			for (int i = 0; i < nRows; i++) {
				boardRender[this.projection][i].setProjection(true);
			}
		}
		else {
			this.projection = position;
			for (int i = 0; i < nCols; i++) {
				boardRender[i][this.projection].setProjection(true);
			}
		}
	}

	/**
	 * Update cell where move has been made
	 * @param point
	 * @param player
	 */
	public void makeMove(Point p, Player player) {
		boardRender[p.x][p.y].setType(player);
	}

	/**
	 * Update entire board
	 * @param board
	 */
	public void updateBoard(Board b) {
		Player[][] board = b.getBoard(); 
		for (int y = nRows-1; y >= 0; y--) {
			for (int x = 0; x < nCols; x++) {
				boardRender[x][y].setProjection(false);
				boardRender[x][y].setType(board[x][y]);
			}
		}
	}
}
