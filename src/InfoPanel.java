import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * InfoPanel for game status
 * InfoPanel is placed alongside GamePanel
 */
public class InfoPanel extends JPanel {
	private JLabel modeLabel;
	private JLabel playerLabel;
	private JLabel statusLabel;
	
	/**
	 * InfoPanel constructor
	 * @param AI
	 * @param player
	 */
	public InfoPanel(boolean AI, Player p) {
		setLayout(new GridLayout(3, 1));
		setBackground(Color.BLACK);
		setPreferredSize(new Dimension(150,600));
		
		modeLabel = new JLabel();
		modeLabel.setText("");
		modeLabel.setForeground(Color.white);
		modeLabel.setVerticalTextPosition(JLabel.CENTER);
		modeLabel.setVerticalAlignment(JLabel.TOP);
		modeLabel.setHorizontalTextPosition(JLabel.CENTER);
		add(modeLabel);
		
		playerLabel = new JLabel();
		playerLabel.setText("");
		playerLabel.setForeground(Color.white);
		playerLabel.setVerticalTextPosition(JLabel.CENTER);
		playerLabel.setVerticalAlignment(JLabel.TOP);
		playerLabel.setHorizontalTextPosition(JLabel.CENTER);
		add(playerLabel);
		
		statusLabel = new JLabel();
		statusLabel.setText("");
		statusLabel.setForeground(Color.white);
		statusLabel.setVerticalTextPosition(JLabel.CENTER);
		statusLabel.setVerticalAlignment(JLabel.TOP);
		statusLabel.setHorizontalTextPosition(JLabel.CENTER);
		add(statusLabel);
	}
	
	/**
	 * Update panel for new player turn
	 * @param player
	 */
	public void updatePlayer(Player p) {
		if (p == Player.PLAYER1) {
			playerLabel.setText("<html><b style='color:red'>Player 1</b> your turn</html>");
		} else if (p == Player.HUMAN) {
			playerLabel.setText("<html><b style='color:red'>Humans'</b> turn</html>");
		} else if (p == Player.PLAYER2) {
			playerLabel.setText("<html><b style='color:yellow'>Player 2</b> your turn</html>");
		} else if (p == Player.CPU) {
			playerLabel.setText("<html><b style='color:yellow'>Aliens'</b> turn</html>");
		}
	}
	
	/**
	 * Update panel to indicate current player made bad move
	 * @param player
	 */
	public void badMove(Player p) {
		if (p == Player.PLAYER1) {
			playerLabel.setText("<html><b style='color:red'>Player 1</b> your turn<p>Bad move. Try again</p></html>");
		} else if (p == Player.HUMAN) {
			playerLabel.setText("<html><b style='color:red'>Humans'</b> turn<p>Bad move. Try again</p></html>");
		} else if (p == Player.PLAYER2) {
			playerLabel.setText("<html><b style='color:yellow'>Player 2</b> your turn<p>Bad move. Try again</p></html>");
		} else if (p == Player.CPU) {
			playerLabel.setText("<html><b style='color:yellow'>Aliens'</b> turn<p>Bad move. Try again</p></html>");
		}
	}
	
	/**
	 * Update statis section of infoPanel
	 * @param direction
	 * @param nextDirection
	 * @param numTurns till next gravity
	 * @param gravity
	 */
	public void updateStatus(int direction, int nextDirection, int numTurns, boolean gravity) {
		if (gravity) {
			String label = "<html><p style='font-size:17px'>Gravity faces <b>";
			switch (direction) {
			case 1: label+="down &darr;"; break;
			case 2: label+="right &rarr;"; break;
			case 3: label+="up &uarr;"; break;
			case 4: label+="left &larr;"; break;
			}
			label+= "</b></p><p>for " + numTurns + " more turn";
			if (numTurns >= 1) label+="s";
			label+=".</p><p></p><p style='font-size:13px'>The next direction is <b>";
			switch (nextDirection) {
			case 1: label+="down &darr;"; break;
			case 2: label+="right &rarr;"; break;
			case 3: label+="up &uarr;"; break;
			case 4: label+="left &larr;"; break;
			}
			label+="</b></p></html>";
			statusLabel.setText(label);
		}
	}
	
	/**
	 * Update panel to display newly selected modes
	 * @param AI
	 * @param gravity
	 * @param randomGrav 
	 */
	public void updateMode(boolean AI, boolean gravity, boolean randomGrav) {
		String label = "<html><p style='font-size:20px'>";
		if (AI) {
			label += "Humans</p><p style='font-size:20px'>vs</p><p style='font-size:20px'>Aliens</p>";
		} else {
			label += "Human</p><p style='font-size:20px'>vs</p><p style='font-size:20px'>Human</p>";
		}
		if (gravity) {
			label += "<p style='font:impact'>Gravity Mode</p>";
			if (randomGrav) label += "<p style='font:impact'>Randomised Gravity</p>";
		}
		label+="</html>";
		modeLabel.setText(label);
	}
}
