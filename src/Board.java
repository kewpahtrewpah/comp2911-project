import java.awt.Point;

/**
 * Board class containing array of Player pieces, board dimensions, gravity direction
 * Logic for validity of moves is done here along with updating board
 */
public class Board {
	private Player board[][];
	private int turn = 0;
	private int xMax;
	private int yMax;
	private int direction;
	private int nextDirection;
	
	/**
	 * Board constructor
	 * @param nCol number of columns
	 * @param nRow number of rows
	 */
	public Board(int nCol, int nRow) {
		this.xMax = nCol;
		this.yMax = nRow;
		this.direction = 1;
		this.nextDirection = 2;
		this.board = new Player[nCol][nRow];
		for (int x = 0; x < nCol; x++) {
			for (int y = 0; y < nRow; y++) {
				this.board[x][y] = Player.NONE;
			}
		}
	}
	
	/**
	 * New Game, clear board
	 * @param nCol
	 * @param nRow
	 */
	public void newGame(int nCol, int nRow) {
		this.xMax = nCol;
		this.yMax = nRow;
		this.board = new Player[nCol][nRow];
		this.turn = 0;
		for (int x = 0; x < nCol; x++) {
			for (int y = 0; y < nRow; y++) {
				this.board[x][y] = Player.NONE;
			}
		}
	}
	
	/**
	 * Set new dimensions
	 * @param nCol
	 * @param nRow
	 */
	public void setDimensions(int nCol, int nRow) {
		this.xMax = nCol;
		this.yMax = nRow;
	}
	
	//if there is any empty spot in the row/column, the move is valid.
		public boolean isValidMove(int e) {
			if (this.direction % 2 == 0) { //gravity is horizontal, check for empty row
				for (int x = 0; x < this.xMax; x++) {
					if (board[x][e] == Player.NONE) return true;
				}
			} else if (direction%2 == 1) { //gravity is vertical, check for empty column
				for (int y = 0; y < this.yMax; y++) {
					if (board[e][y] == Player.NONE) return true;
				}
			}
			return false;
		}
	
	public void undoMove(Point p) {
		if (p.x >= 0 && p.y >= 0) this.board[p.x][p.y] = Player.NONE;
	}
	
	public int getNCol() {
		return this.xMax;
	}
	
	public int getNRow() {
		return this.yMax;
	}
	
	public int getTurn() {
		return this.turn;
	}
	
	public int getDirection() {
		return this.direction;
	}
	
	public void setDirection(int direction) {
		this.direction = direction;
	}
	
	public int getNextDirection() {
		return this.nextDirection;
	}
	
	public void setNextDirection(int nextDir) {
		this.nextDirection = nextDir;
	}
	
	public void incrementTurn() {
		this.turn++;
	}
	
	/**
	 * Get board array
	 * @return board array
	 */
	public Player[][] getBoard() {
		return board;
	}
	
	/**
	 * Make a move at position
	 * returns Point(-1,-1) if badMove
	 * @param position
	 * @param p player
	 * @return Point where move was made
	 */
	public Point makeMove(int position, Player p) {
		
		/*
		 * If the bottom is empty, fill the bottom.
		 * If the top is full, move is invalid
		 * Else scan through and find the suitable position
		 */
		int x, y;
		if (direction == 1) {
			if (this.board[position][0] == Player.NONE) { //fills the bottom spot if it's empty
				this.board[position][0] = p;
				return new Point(position,0);
			} else if (this.board[position][this.yMax-1] == Player.NONE) { //checks that the column isnt full
				for (y = 0; y < this.yMax; y++) {
					if (this.board[position][y] == Player.NONE) {
						this.board[position][y] = p;
						return new Point(position,y);
					}
				}
			return new Point(-1,-1);
			}
		}
		if (direction == 4) {
			if (this.board[0][position] == Player.NONE) { //fills the bottom spot if it's empty
				this.board[0][position] = p;
				
				return new Point(0,position);
			} else if (this.board[this.xMax-1][position] == Player.NONE) { //checks that the row isnt full
				for (x = 0; x < this.xMax; x++) {
					if (this.board[x][position] == Player.NONE) {
						this.board[x][position] = p;
						
						return new Point(x,position);
					}
				}
			return new Point(-1,-1);
			}

		}
		if (direction == 3) {
			if (this.board[position][this.yMax-1] == Player.NONE) { //fills the bottom spot if it's empty
				this.board[position][this.yMax-1] = p;
				
				return new Point(position, this.yMax-1);
			} else if (this.board[position][0] == Player.NONE) { //checks that the column isnt full
				for (y = this.yMax-1; y >= 0; y--) {
					if (this.board[position][y] == Player.NONE) {
						this.board[position][y] = p;
						
						return new Point(position,y);
					}
				}
				return new Point(-1,-1);
			}
		}
		if (direction == 2) {
			if (this.board[this.xMax-1][position] == Player.NONE) { //fills the bottom spot if it's empty
				this.board[this.xMax-1][position] = p;
				
				return new Point(this.xMax-1, position);
			} else if (this.board[0][position] == Player.NONE) { //checks that the column isnt full
				for (x = this.xMax-1; x >= 0; x--) {
					if (this.board[x][position] == Player.NONE) {
						this.board[x][position] = p;
						
						return new Point(x,position);
					}
				}
				return new Point(-1,-1);
			}
		}
		return new Point(-1,-1);
	}
	
	/**
	 * Check if player has won
	 * @param p player
	 * @return true if player has won
	 */
	public Boolean checkWin(Player p) {
		//horizontal
		Boolean started = false;
		int count = 0;
		for (int y = 0; y < yMax; y++) {
			for (int x = 0; x < xMax; x++) {
				if (this.board[x][y] == p) {
					if (!started) {
						started = true;
						count++;
					} 
					else count++;
				} 
				else {
					if (started) break; 
				}
			}
			if (count >= 4) return true;
			else {
				started = false;
				count = 0;
			}
		}
		
		//vertical
		for (int x = 0; x < xMax; x++) {
			for (int y = 0; y < yMax; y++) {
				if (this.board[x][y] == p) {
					if (!started) {
						started = true;
						count++;
					} 
					else count++;
				} else {				
					if (started) break;
				}
			}
			if (count >= 4) return true;
			else {
				started = false;
				count = 0;
			}
		}
		
		//diagonal TR
		for (int y = 0; y < this.yMax - 3; y++) {
			for (int x = 0; x < this.xMax - 3; x++) {
				if (board[x][y] == p) count++;
				if (board[x + 1][y + 1] == p) count++;
				if (board[x + 2][y + 2] == p) count++;
				if (board[x + 3][y + 3] == p) count++;
				if (count == 4) { return true; }
				else { count = 0; }
			}
		}
			
		//diagonal TL 
		for (int y = 0; y < this.yMax - 3; y++) {
			for (int x = 3; x < this.xMax; x++) {
				if (board[x][y] == p) count++;
				if (board[x - 1][y + 1] == p) count++;
				if (board[x - 2][y + 2] == p) count++;
				if (board[x - 3][y + 3] == p) count++;
				if (count == 4) { return true; }
				else { count = 0; }
			}
		}
		
		return false;
	}
	
	/**
	 * Shift board pieces in specified direction
	 * @param direction
	 */
	public void shift(int direction) {
		//now shift pieces to new gravity direction
		if (direction == 1) { //down
			for (int x = 0; x < this.xMax; x++) {
				//go through each column, if there is a gap in front of the counter, move it down.
				//do this for all counters
				boolean incomplete = true;
				while (incomplete) {
					incomplete = false;
					for (int y = 1; y < this.yMax; y++) {
						if (this.board[x][y] != Player.NONE && this.board[x][y-1] == Player.NONE) {
							this.board[x][y-1] = this.board[x][y];
							this.board[x][y] = Player.NONE;
							incomplete = true;
						}
					}
				}
			}
		} else if (direction == 3) {
			for (int x = 0; x < this.xMax; x++) {
				//go through each column, if there is a gap in front of the counter, move it down.
				//do this for all counters
				boolean incomplete = true;
				while (incomplete) {
					incomplete = false;
					for (int y = this.yMax-2; y >= 0; y--) {
						if (this.board[x][y] != Player.NONE && this.board[x][y+1] == Player.NONE) {
							this.board[x][y+1] = this.board[x][y];
							this.board[x][y] = Player.NONE;
							incomplete = true;
						}
					}
				}
			}
		} else if (direction == 2) {
			for (int y = 0; y < this.yMax; y++) {
				//go through each column, if there is a gap in front of the counter, move it down.
				//do this for all counters
				boolean incomplete = true;
				while (incomplete) {
					incomplete = false;
					for (int x = this.xMax-2; x >= 0; x--) {
						if (this.board[x][y] != Player.NONE && this.board[x+1][y] == Player.NONE) {
							this.board[x+1][y] = this.board[x][y];
							this.board[x][y] = Player.NONE;
							incomplete = true;
						}
					}
				}
			}
		} else if (direction == 4) {
			for (int y = 0; y < this.yMax; y++) {
				//go through each column, if there is a gap in front of the counter, move it down.
				//do this for all counters
				boolean incomplete = true;
				while (incomplete) {
					incomplete = false;
					for (int x = 1; x < this.xMax; x++) {
						if (this.board[x][y] != Player.NONE && this.board[x-1][y] == Player.NONE) {
							this.board[x-1][y] = this.board[x][y];
							this.board[x][y] = Player.NONE;
							incomplete = true;
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * Check if board is filled
	 * @return true if full
	 */
	public boolean isFull() {
		boolean full = true;
		for (int x = 0; x < this.xMax; x++) {
			for (int y = 0; y < this.yMax; y++) {
				if (this.board[x][y] == Player.NONE) {
					full = false;
				}
			}
		}
		
		return full;
	}
}
